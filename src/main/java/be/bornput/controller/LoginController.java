package be.bornput.controller;

import be.bornput.model.LoginForm;
import be.bornput.service.LoginService;

public class LoginController {

    private LoginService loginService;

    public LoginController(LoginService loginService) {
        this.loginService = loginService;
    }

    public String login(LoginForm userForm){
        if(null == userForm){
            return "ERROR";
        }else{
            boolean logged;

            try {
                logged = loginService.login(userForm);
            } catch (Exception e) {
                return "ERROR";
            }

            if(logged){
                loginService.setCurrentUser(userForm.getUserName());
                return "OK";
            }else{
                return "KO";
            }
        }
    }
}
