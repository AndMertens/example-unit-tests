package be.bornput.error;

public class InvalidLoginException extends Throwable {

    public static final String INVALID_PASSWORD =  "Invalid password : length = 8 characters or longer, contain upper and lower case and" +
            "at least 1 numeric and special char.";

    public InvalidLoginException(String s) {
        super (s);
    }
}
