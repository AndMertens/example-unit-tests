package be.bornput.service;

import be.bornput.dao.LoginDAO;
import be.bornput.model.LoginForm;

public class LoginService {

    private final LoginDAO loginDAO;
    private String currentUser;

    public LoginService(LoginDAO loginDAO) {
        this.loginDAO = loginDAO;
    }

    public boolean login(LoginForm userForm) {
        assert null  != userForm;
        int loginResult = loginDAO.LogIn(userForm);

        switch (loginResult) {
            case 1:
                return true;
            default:
                return false;
        }
    }

    public void setCurrentUser(String userName){
        this.currentUser = userName;
    }

}
