package be.bornput.model;

import be.bornput.error.InvalidLoginException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoginForm {

    private String login;
    private String password;

    public LoginForm(String login, String password)throws InvalidLoginException {
        setUserName(login);
        setLoginPassword(password);
    }

    public LoginForm() { }

    public String getUserName() {
        return this.login;
    }


    public String getLoginPassword() {
        return this.password;
    }

    public void setUserName(String userName) {
        if(userName == null) throw new NullPointerException("Invalid login : [" + userName + "].");
        this.login = userName;

    }

    public void setLoginPassword(String password) throws InvalidLoginException{
        if(password == null) throw new NullPointerException("Invalid password : [" + password + "].");
        if(!validatePassword(password))
            throw new InvalidLoginException(InvalidLoginException.INVALID_PASSWORD );
        this.password = password;

    }

    private boolean validatePassword(String password){
        Pattern p = Pattern.compile("[a-zA-Z]*(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%&+=]).{8,}$");
        Matcher m = p.matcher(password);
        return m.matches();
    }
}
