package be.bornput.service;

import be.bornput.model.LoginForm;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertFalse;

@RunWith(MockitoJUnitRunner.class)
public class LoginServiceWithMockingTest {

    @Mock
    private LoginForm myLoginForm;

    @InjectMocks
    private LoginService myLoginService;

    @Test()
    public void testLogInService() {
        assertFalse(myLoginService.login(myLoginForm));
    }

}
