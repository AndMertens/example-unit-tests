package be.bornput.service;

import be.bornput.dao.LoginDAO;
import be.bornput.error.InvalidLoginException;
import be.bornput.model.LoginForm;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


@RunWith(JUnit4.class)
public class LoginServiceWithoutMockingTest {

    private LoginForm myLoginForm;
    private LoginDAO myLoginDAO;


    @Before
    public void init() throws InvalidLoginException {
       myLoginDAO = new LoginDAO();
       myLoginForm = new LoginForm("andymert","Valid@Pasword1");
    }

    @Test()
    public void testLogInService() {
        LoginService myLoginService = new LoginService(myLoginDAO);
       assertFalse(myLoginService.login(myLoginForm));
    }

}
