package be.bornput.dao;

import be.bornput.error.InvalidLoginException;
import be.bornput.model.LoginForm;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertEquals;


@RunWith(JUnit4.class)
public class LoginDAOTest {

    @Test()
    public void testValidPasswordAccordingRegex()throws InvalidLoginException {
        LoginDAO daoLogin = new LoginDAO();
        assertEquals(0, daoLogin.LogIn(new LoginForm("test","Valid@pass1")));
    }

}
