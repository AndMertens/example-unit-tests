package be.bornput.controller;

import be.bornput.dao.LoginDAO;
import be.bornput.model.LoginForm;
import be.bornput.service.LoginService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class LoginControllerWithMockingTest {

    @Mock
    private LoginForm myLoginForm;

    @Mock
    private LoginService myLoginService;

    @InjectMocks
    private LoginController myLoginController;

    @Test()
    public void testLogInControllerCorrect() {
        when(myLoginService.login(myLoginForm)).thenReturn(true);
        myLoginController = new LoginController(myLoginService);
        assertEquals("OK", myLoginController.login(myLoginForm));
    }

    @Test()
    public void testLogInControllerInCorrect() {
        myLoginController = new LoginController(myLoginService);
        assertEquals("KO", myLoginController.login(myLoginForm));
    }

    @Test()
    public void testLogInControllerNullError() {
        assertEquals("ERROR", myLoginController.login(null));
    }

}
