package be.bornput.controller;

import be.bornput.dao.LoginDAO;
import be.bornput.error.InvalidLoginException;
import be.bornput.model.LoginForm;
import be.bornput.service.LoginService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class LoginControllerWithoutMockingTest {

    private LoginService logInService;
    private LoginForm userForm;

    @Before
    public void init() throws InvalidLoginException {
        userForm = new LoginForm("andymert", "Valid@Password1");
        logInService = new LoginService(new LoginDAO());
    }

    @Test
    public void LoginControllerWithValidUserForm(){
        LoginController loginController = new LoginController(logInService);
        assertEquals("OK",loginController.login(userForm));
    }
}
