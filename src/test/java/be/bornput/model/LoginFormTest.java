package be.bornput.model;

import be.bornput.error.InvalidLoginException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.Assert.assertEquals;


@RunWith(JUnit4.class)
public class LoginFormTest {

    final String INVALID_LOGIN_ERROR_MESSAGE = "Invalid password : length = 8 characters or longer, contain upper and lower case and" +
            "at least 1 numeric and special char.";

    @Test
    public void testCorrectLogIn()throws InvalidLoginException{
        LoginForm logInForm = new LoginForm();
        logInForm.setUserName("andymert");
        logInForm.setLoginPassword("Puty1@tput");

        assertEquals("andymert",logInForm.getUserName());
        assertEquals("Puty1@tput",logInForm.getLoginPassword());

    }

    @Test()
    public void testLoginWithNullPointerExceptionForPassword()throws InvalidLoginException{
        LoginForm logInForm = new LoginForm();
        assertThatThrownBy(() -> logInForm.setLoginPassword(null))
                .isInstanceOf(NullPointerException.class)
                .hasMessageContaining("Invalid password : [null].");


    }

    @Test()
    public void testLoginWithNullPointerExceptionForLogin(){
        LoginForm logInForm = new LoginForm();
        assertThatThrownBy(() -> logInForm.setUserName(null))
                .isInstanceOf(NullPointerException.class)
                .hasMessageContaining("Invalid login : [null].");


    }

    @Test()
    public void testInvalidPasswordLacksNumber(){

        LoginForm logInForm = new LoginForm();
        assertThatThrownBy(() -> logInForm.setLoginPassword("test@Pass"))
                .isInstanceOf(InvalidLoginException.class)
                .hasMessageContaining(INVALID_LOGIN_ERROR_MESSAGE);
    }

    @Test()
    public void testInvalidPasswordLacksSpecialCharacter(){

        LoginForm logInForm = new LoginForm();
        assertThatThrownBy(() -> logInForm.setLoginPassword("test1Pass"))
                .isInstanceOf(InvalidLoginException.class)
                .hasMessageContaining(INVALID_LOGIN_ERROR_MESSAGE);
    }

    @Test()
    public void testInvalidPasswordLacksUppercase(){

        LoginForm logInForm = new LoginForm();
        assertThatThrownBy(() -> logInForm.setLoginPassword("test1@pass"))
                .isInstanceOf(InvalidLoginException.class)
                .hasMessageContaining(INVALID_LOGIN_ERROR_MESSAGE);
    }

    @Test()
    public void testInvalidPasswordLackslowercase(){

        LoginForm logInForm = new LoginForm();
        assertThatThrownBy(() -> logInForm.setLoginPassword("TEST1@PASS"))
                .isInstanceOf(InvalidLoginException.class)
                .hasMessageContaining(INVALID_LOGIN_ERROR_MESSAGE);
    }

    @Test()
    public void testInvalidPasswordLength(){

        LoginForm logInForm = new LoginForm();
        assertThatThrownBy(() -> logInForm.setLoginPassword("Test1@"))
                .isInstanceOf(InvalidLoginException.class)
                .hasMessageContaining(INVALID_LOGIN_ERROR_MESSAGE);
    }


    @Test()
    public void testValidPasswordAccordingRegex()throws InvalidLoginException {
        LoginForm logInForm = new LoginForm();
        logInForm.setLoginPassword("ValidPaswordForRegex@123");
    }

}
